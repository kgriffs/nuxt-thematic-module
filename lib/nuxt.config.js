const path = require('path')

const MinifyPlugin = require('babel-minify-webpack-plugin')
// const UglifyJSPlugin = require('node_modules/uglifyjs-webpack-plugin')
const webpack = require('webpack')


const TARGET_EVERGREEN = (process.env.BROWSER_TARGET == 'evergreen')


module.exports = {
  build: {
    createRoutes () {
      // NOOP: Disables check for 'pages' directory
    },

    extend (config, ctx) {

      if (ctx.isClient) {

        function disableUglify() {
          config.plugins = config.plugins.filter(p => !(p instanceof webpack.optimize.UglifyJsPlugin))
        }

        if (ctx.isDev) {
          // Don't do any minification in dev mode
          disableUglify()

          // Lint the code
          config.module.rules.push({
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /(node_modules)/
          })
        }
        else {
          if (TARGET_EVERGREEN) {
            // Use babel-minify to handle ES6+, since the stable release of
            // UglifyJS currently only understands ES5.
            disableUglify()

            config.plugins.push(new MinifyPlugin(
              {},
              {
                sourceMap: false
              }
            ))
          }
          else {
            // NOOP: Use default Uglify configuration
          }
        }
      }
    },

    babel: getBabelOptions()
  },

  extractCSS: true,

  modules: [
    '~/modules/lithography'
  ],

  // https://github.com/kangax/html-minifier
  minify: {
    collapseBooleanAttributes: true,
    collapseWhitespace: true,
    decodeEntities: true,
    minifyCSS: true,
    minifyJS: true,
    processConditionalComments: true,
    removeAttributeQuotes: false,
    removeComments: true,
    removeEmptyAttributes: true,
    removeOptionalTags: true,
    removeRedundantAttributes: true,
    removeScriptTypeAttributes: false,
    removeStyleLinkTypeAttributes: false,
    removeTagWhitespace: false,
    sortAttributes: true,
    sortClassName: false,
    trimCustomFragments: true,
    useShortDoctype: true
  }
}


function getBabelOptions() {
  let targets = { ie: 9, forceAllTransforms: true }

  if (TARGET_EVERGREEN) {
    targets = {
      browsers: [
        'last 2 Chrome versions',
        'last 2 Safari versions',
        'last 2 iOS versions',
        'last 2 Firefox versions',
        'last 2 Edge versions', 'not Edge < 15'
      ]
    }
  }

  return {
    presets: [
      'stage-2',
      ['vue-app', {
        targets,
        useBuiltIns: TARGET_EVERGREEN
      }]
    ],
    plugins:
    [
      'lodash'
    ]
  }
}